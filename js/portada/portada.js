// documento on ready
$(function() {
    // Carga la hora actual y la fecha y se actualiza cada segundo
    setInterval(function() {
        let fecha = new Date();
        let dia = fecha.getDate();
        let mes = fecha.getMonth();
        let anio = fecha.getFullYear();
        let hora = fecha.getHours();
        let minutos = fecha.getMinutes();
        let segundos = fecha.getSeconds();
    
        // Muestra la hora en una caja de texto
        $('.es-hora').html((hora-1) + ':' + minutos + ':' + segundos);
        $('.es-fecha').html(dia + '/' + mes + '/' + anio);
    }, 1000);

})

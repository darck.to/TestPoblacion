// documento on ready
$(function() {
    // Carga CSV Archivo 
    $('.carga-csv').on('change',function() {
        console.log(1)
        loadCSVFileCheck($(this));
    });
    
    // Guarda los datos en la base de datos
    $('#btn-guardar').on('click', function(e) {
        let content = exportObject.data;
        content = content.filter(val => val)
        let header = content.shift();
        let result = [];
        // Recorre el array de datos y agreaga los headers a los values
        content.forEach(function(values) {
            let obj = {};
            for(let i = 0; i < header.length; i++) {
                obj[header[i]] = values[i];
            }
            result.push(obj);
        });
        // Remueve los valores vacios
        removeEmptyValues(result).then(function(result) {
            //console.log(result);
        }).catch(function(error) {
            console.log(error);
        });
        // Guarda en firebase
        $.each(result, function(index, value) {
            // Nombre y carpeta
            set(ref(db, jsonName + "/" + index),{
                // Aqui van los campos de la tabla
                value
            })
            .then(()=>{
                toast("Data added successfully");
            })
            .catch((error)=>{
                toast(error, "danger");
            });
        })
    })
    
    // Inicializa el result como array y el fileName
    let exportObject = undefined;
    let jsonName = '';

    // Revisa el archivo CSV y lo carga en el DOM
    function loadCSVFileCheck(fileInput) {
        jsonName = '';
        let fileName = fileInput.val();
        let extension = fileName.split('.').pop().toLowerCase();
        let names = {
            'TOTL':'<span class="tag m-1 is-danger">Todas las Edades</span>',
            'Metadata':'<span class="tag m-1 is-warning">MetaDatos</span>',
            'TO_':'<span class="tag m-1 is-danger">Poblacion Total</span>',
            '0014':'<span class="tag m-1 is-info">edad 0 a 14</span>',
            '1564':'<span class="tag m-1 is-info">edad 15 a 64</span>',
            '65UP':'<span class="tag m-1 is-info">edad 65 y m&aacute;s</span>',
            'FE':'<span class="tag m-1 is-success">Femenina</span>',
            'MA':'<span class="tag m-1 is-success">Masculina</span>',
            'IN':'<span class="tag m-1 is-warning">Ingresos</span>'
        };
        let cabeceraTexto = 'Basado en el titulo parece contener:';
        $.each(names, function(index, value) {
            if (fileName.indexOf(index) != -1) {
                cabeceraTexto += value;
                jsonName += '_' + index;
            }
        })
        $('.card-header-title').html(cabeceraTexto)

        if(extension != 'csv') {
            toast('Solo CSV','danger');
            $('.file-name').html('<span class="has-text-weight-bold has-text-danger">Archivo no v&aacute;lido<span>');
            $('.boton-guarda').addClass('is-disabled');
            $('#contentReader').html('')
        } else {
            toast('Si es CSV');
            $('.file-name').html('<span class="has-text-weight-bold has-text-success">' + fileName + '</span>');
            $('.boton-guarda').removeClass('is-disabled');
            $('#contentReader').html('');
            // Carga el archivo CSV en JsonFormat
            loadCSVFileDOM();
        }
    }

    // Carga el CSV en el DOM y los conveirte en JSON
    function loadCSVFileDOM(e) {
        let file = $('.carga-csv').prop('files')[0];
        let txt = ''
        Papa.parse(file, {
            complete: function(results) {
                // Guarda los resultados en el array exportObject
                exportObject = results
                $.each(exportObject.data, function(index, value) {
                    // Creamos el texto a mostrar
                    txt += '<p class="is-size-7 paraIndex-' + index + '">';
                        txt += '<span class="icon has-text-danger-dark is-clickable borra-registro" data-id="' + index + '"><ion-icon name="remove-circle"></ion-icon></span>';
                    $.each(value, function(key, item) {
                        txt += '<span>' + item + ', </span>'
                    })
                    txt += '</p>'
                })
                // parse json to string
                //jsonResults = JSON.stringify(results, null, "\t");
                // Cambiamos el tema del div
                $('.cargar-content-show').css('height', '250px');
                $('#contentReader').html(txt);

                $('.borra-registro').on('click', function(e) {
                    let id = $(this).data('id');
                    // Crea un modal para confirmar el borrado
                    let mTxt = '<div class="card-header has-text-centered">';
                        mTxt += '<span class="card-header-title">Quieres borrar el registro ' + id + '?</span>'
                    mTxt += '</div>';
                    mTxt += '<div class="card-content has-text-centered">';
                        mTxt += '<button class="button is-danger" id="borrarRegistro">Borrar!</button>';
                    mTxt += '</div>';
                    modal(mTxt);
                    // Si se confirma el borrado
                    $('#borrarRegistro').on('click', function(e) {
                        if (delete exportObject.data[id]) {
                            toast('Registro borrado', 'success');
                            $('.paraIndex-' + id).remove();
                        } else {
                            toast('No se pudo borrar el registro', 'danger');
                            return false;
                        }
                        modax();
                        //console.log(exportObject.data);
                    })
                })
            }
        })
    }

    // Remueve los datos vacios del objeto
    function removeEmptyValues(obj) {
        return new Promise(function(resolve, reject) {
            let keys = Object.keys(obj);
            keys.forEach(function(key) {
                if (typeof obj[key] === 'object') {
                    removeEmptyValues(obj[key]).then(function(result) {
                        obj[key] = result;
                    });
                } else if (obj[key] === '') {
                    delete obj[key];
                }
            });
            resolve(obj);
        });
    }
}());